#pragma once
#include <stdbool.h>
#include <assert.h>

#define boardlength 8
#define boardsize (boardlength * boardlength) 
#define lookahead 5

typedef enum { Black = -1, White = 1 } Player;
typedef enum { Pawn, Rook, Knight, Bishop, Queen, King } Piece;
#define npiecetypes 6

/* As `Player` has no zero value, `Unit` as defined here can never be zero.
 * Instead, a block of memory which has a zero value, corresponding to a
 * `Unit`, will be interpreted as an empty space. */
typedef struct {
	Player owner;
	Piece piece;
} Unit;

typedef struct {
	Unit contents[boardsize];
} Board;

typedef struct {
	int nmoves;
	int *loc;
	int *moves;
} Moveset;

/* All girls are valid. Not all indices are. */
inline bool valid(int r, int c) {
	return (r > 0 && r < boardlength && c > 0 && c < boardlength);
}

inline int toidx(int r, int c) {
	assert (valid(r, c));
	return r * boardlength + c;
}

inline const Unit *cidx(const Board *board, int r, int c) {
	assert (valid(r, c));
	return &board->contents[toidx(r, c)];
}

inline Unit *idx(Board *board, int r, int c) {
	assert (valid(r, c));
	return &board->contents[toidx(r, c)];
}

inline bool occupied(const Board *board, int r, int c) {
	return cidx(board, r, c)->owner;
}

int availablemovesat(int **moves, const Board *board, int r, int c);

int availablemoves(Moveset **moves, Player turn, const Board *board);

/* Returns a pair of booleans in the least-significant bits.
 * 	retval & 1 =:= white is in check/checkmate
 * 	retval & 2 =:= black is in check/checkmate
 */
int check(const Board *board);

int checkmate(const Board *board);
