#pragma once

#include "core.h"

#define TREEWIDTH 50 /* the maximum possible width of the move tree down to LOOKAHEAD levels deep */

typedef struct {
	int values[npiecetypes];
} Values;

double score(const Board *board, const Values *values);

void bestmove(Board *dst, const Board *board, Player turn, const Values *white, const Values *black);
