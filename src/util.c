#include <stdlib.h>

void free_Moveset(Moveset *ms) {
	int *moves;

	for (moves = ms->moves; ms->nmoves; --ms, ++moves)
		free(moves);

	free(ms);
}
