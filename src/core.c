#include "core.h"
#include "util.h"
#include <stdlib.h>
#include <string.h>

#define MAX_PAWNMOVES 3
#define MAX_ROOKMOVES ((boardlength - 1) * 2)
#define MAX_KNIGHTMOVES 8
/* TODO check that this is really the maximum number of
 * possible moves for a bishop */
#define MAX_BISHOPMOVES (boardlength * 2 - 3)
#define MAX_QUEENMOVES (MAX_ROOKMOVES + MAX_BISHOPMOVES)
#define MAX_KINGMOVES 8

/* A move which does not actually move anything is nonsensical (chess does not
 * allow for passes), so zero is a safe sentinel value of sorts to represent
 * failure, while also conveniently acting as a falsy value in conditionals */
static int moveto(const Board *board, int r, int c) {
	if (valid(r, c) && !occupied(board, r, c))
		return toidx(r, c);
	else
		return 0;
}

static int attackto(const Board *board, Player owner, int r, int c) {
	if (valid(r, c)
	 && occupied(board, r, c)
	 && owner != cidx(board, r, c)->owner)
		return toidx(r, c);
	else
		return 0;
}

int availablemovesat(int **moves, const Board *board, int r, int c) {
#define moves (*moves) /* Fake reference */
	/* Meant to be inside a `for` loop, MOVE_CONTINUOUS generalizes the way
	 * that most chess pieces move: if it can keep going in the same
	 * direction, it may do so; if it finds another piece in its way, it
	 * may capture it if belonging to the opposing player, or stop just
	 * before otherwise, and go no farther in either case. */
#define MOVE_CONTINUOUS(r, c) do { \
	if (( moves[total] = attackto(board, unit->owner, (r), (c)) )) { \
		++total; \
		break;   \
	}                \
	if (( moves[total] = moveto(board, (r), (c)) )) \
		++total; \
	else             \
		break;   \
} while (0)
#define MOVE_OR_CAPTURE(r, c) do { \
	if (( moves[total] = (attackto(board, unit->owner, (r), (c)) \
	                   || moveto(board, (r), (c))) )) \
		++total; \
} while (0)

	int total, i, rsgn, csgn;
	const Unit *unit;

	total = 0;
	unit = cidx(board, r, c);

	switch (unit->piece) {
	case Pawn:
		moves = malloc(sizeof(int) * MAX_PAWNMOVES);

		if (( moves[total] = moveto(board, r + unit->owner, c) ))
			++total;
		if (( moves[total] = attackto(board, unit->owner, r + unit->owner, c - 1) ))
			++total;
		if (( moves[total] = attackto(board, unit->owner, r + unit->owner, c + 1) ))
			++total;
		break;

	case Rook:
		moves = malloc(sizeof(int) * MAX_ROOKMOVES);

		/* Move west */
		for (i = c - 1; i; --i)
			MOVE_CONTINUOUS(r, i);
		/* Move east */
		for (i = c + 1; i < boardlength; ++i)
			MOVE_CONTINUOUS(r, i);
		/* Move south */
		for (i = r - 1; i; --i)
			MOVE_CONTINUOUS(i, c);
		/* Move north */
		for (i = r + 1; i < boardlength; ++i)
			MOVE_CONTINUOUS(i, c);
		break;

	case Knight:
		moves = malloc(sizeof(int) * MAX_KNIGHTMOVES);

		for (i = 0; i < 4; ++i) {
			rsgn = (i & 1) * 2 - 1;
			csgn = (i & 2) * 2 - 1;
			/* The following code will execute for all (rsgn, csgn)
			 * in {-1, 1} x {-1, 1} where x is the Cartesian product */
			MOVE_OR_CAPTURE(r + rsgn, c + csgn * 2);
			MOVE_OR_CAPTURE(r + rsgn * 2, c + csgn);
		}
		break;

	case Bishop:
		moves = malloc(sizeof(int) * MAX_BISHOPMOVES);

		/* Move northeast */
		for (i = 1; r + i < boardlength && c + i < boardlength; ++i)
			MOVE_CONTINUOUS(r + i, c + i);
		/* Move northwest */
		for (i = 1; r + i < boardlength && c + i; ++i)
			MOVE_CONTINUOUS(r + i, c - i);
		/* Move southeast */
		for (i = 1; r - i && c + i < boardlength; ++i)
			MOVE_CONTINUOUS(r - i, c + i);
		/* Move southwest */
		for (i = 1; r - i && c - i; ++i)
			MOVE_CONTINUOUS(r - i, c - i);
		break;

	case Queen:
		moves = malloc(sizeof(int) * MAX_QUEENMOVES);

		/* Move west */
		for (i = c - 1; i; --i)
			MOVE_CONTINUOUS(r, i);
		/* Move east */
		for (i = c + 1; i < boardlength; ++i)
			MOVE_CONTINUOUS(r, i);
		/* Move south */
		for (i = r - 1; i; --i)
			MOVE_CONTINUOUS(i, c);
		/* Move north */
		for (i = r + 1; i < boardlength; ++i)
			MOVE_CONTINUOUS(i, c);
		/* Move northeast */
		for (i = 1; r + i < boardlength && c + i < boardlength; ++i)
			MOVE_CONTINUOUS(r + i, c + i);
		/* Move northwest */
		for (i = 1; r + i < boardlength && c + i; ++i)
			MOVE_CONTINUOUS(r + i, c - i);
		/* Move southeast */
		for (i = 1; r - i && c + i < boardlength; ++i)
			MOVE_CONTINUOUS(r - i, c + i);
		/* Move southwest */
		for (i = 1; r - i && c - i; ++i)
			MOVE_CONTINUOUS(r - i, c - i);
		break;

	case King:
		moves = malloc(sizeof(int) * MAX_KINGMOVES);

		MOVE_OR_CAPTURE(r    , c - 1);
		MOVE_OR_CAPTURE(r    , c + 1);
		MOVE_OR_CAPTURE(r - 1, c    );
		MOVE_OR_CAPTURE(r + 1, c    );
		MOVE_OR_CAPTURE(r - 1, c - 1);
		MOVE_OR_CAPTURE(r - 1, c + 1);
		MOVE_OR_CAPTURE(r + 1, c - 1);
		MOVE_OR_CAPTURE(r + 1, c + 1);
		break;
	}

	return total;
#undef MOVE_CONTINUOUS
#undef MOVE_OR_CAPTURE
#undef moves
}

int availablemoves(Moveset **moves, Player turn, const Board *board) {
#define moves (*moves) /* Fake reference */
	int npieces, i;

	for (i = 0, npieces = 0; i < boardsize; ++i) {
		if (cidx(board, i / boardlength, i % boardlength)->owner == turn)
			++npieces;
	}

	moves = malloc(sizeof(Moveset*) * npieces);
	for (i = 0; i < boardsize; ++i, ++moves)
		moves->nmoves = availablemovesat(&moves->moves, board,
		                                 i / boardlength, i % boardlength);

	return npieces;
#undef moves
}
