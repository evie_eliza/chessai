#include <string.h>
#include <stdlib.h>

#include "decisions.h"
#include "parameters.h"
#include "core.h"
#include <stdarg.h>

double score(const Board *board, const Values *values) {
	int i;
	double acc;
	const Unit *unit;

	for (i = 0, acc = 0, unit = board->contents; i < boardsize; ++i, ++unit)
		acc += unit->owner * values->values[unit->piece];

	return acc;
}

int genmoves(Board *dst, const Board *board, Player turn) {
	/* TODO */
}

void bestmove(Board *dst, const Board *board, Player turn,
              const Values *white, const Values *black) {
	/* LOOKAHEAD x TREEWIDTH rectangular, circular arrays of the move tree, logically
	 * starting at the row `rowzero`. This structure allows us to reuse the branch we
	 * choose here during the next call of this function, so that we only have to
	 * generate one row of the tree each call.
	 *
	 * ftr_* forms a collection of fields, where ftr_boards[i][j] is a board with
	 * ftr_nchildren[i][j] children (i.e. distinct boardstates that result from a
	 * single additional move), and from which the player whose turn it is will
	 * prefer to move towards `favored`.
	 */
	static int rowzero;
	static Board ftr_boards [LOOKAHEAD * TREEWIDTH];
	static int ftr_nchildren[LOOKAHEAD * TREEWIDTH];
	long ftr_favored      [LOOKAHEAD * TREEWIDTH]; /* I think this is the type of array indices? */
	int tailrow;
	double whitescores[TREEWIDTH], blackscores[TREEWIDTH];

	/* a couple utility aliases for circular arrays */
#define decrement(row) ((row) ? (row) - 1 : LOOKAHEAD - 1)
#define increment(row) ((row) < LOOKAHEAD - 1 ? (row) + 1 : 0)
	/* floating-point infinity is going to be used */
#define INF (1.0 / 0.0)

	tailrow = decrement(rowzero); /* the old tails */

	{ /* first, generate the new "tails", i.e. the furthest-future predictions, which are the result of exactly LOOKAHEAD moves */
		int parent, child;
		/* Overwrite rowzero as the new tail row */
		for (parent = TREEWIDTH * tailrow, child = TREEWIDTH * rowzero;
		     parent < TREEWIDTH * tailrow;
		     ++parent) {
			if (ftr_nchildren[TREEWIDTH * tailrow + parent] == -1) continue;

			ftr_nchildren[parent] = genmoves(&ftr_boards[child], &ftr_boards[parent], LOOKAHEAD & 1 ? -turn : turn);
			child += ftr_nchildren[parent];
		}

		/* We have now generated the new tails; the indices should reflect this fact */
		tailrow = increment(tailrow);
		rowzero = increment(rowzero);
	} { /* Second, assign scores to each cell of the new tail */
		int i;
		for (i = 0; i < TREEWIDTH; ++i) {
			whitescores[i] = score(&ftr_boards[tailrow * TREEWIDTH + i], white);
			blackscores[i] = score(&ftr_boards[tailrow * TREEWIDTH + i], black);
		}
	} { /* Third, iterate backwards from tailrows to rowzero, running a modified minimax to set ftr_favored */
		int parentrow, parentcol, childrow, childcol, i;
		double minmax;
		Player parentturn;

		for (i = 0; i < TREEWIDTH; ++i)
			ftr_favored[TREEWIDTH * tailrow + i] = i;
		
		/* Tangle of iteration variables, readers beware */
		/* +-----------+----------------+--------------+-----------------------------------------------------------------------+
		 * |           | Initialized by | Condition of | Iterated by | Purpose                                                 |
		 * +-----------+----------------+--------------+-------------+---------------------------------------------------------+
		 * | childrow  |  loop A        | Ø            | loop A      | the row one turn futher-future                          |
		 * | parentrow |  loop A        | loop A       | loop A      | the row currently deciding its best moves               |
		 * | childcol  |  loop B        | Ø            | loop C      | the first element of childrow that hasn't been read yet |
		 * | parentcol |  loop B        | loop B       | loop B      | the exact cell currently min/max-scored child           |
		 * | i         |  loop C        | loop C       | loop C      | used to iterate over exactly nchildren children         |
		 * +-----------+----------------+--------------+-------------+---------------------------------------------------------+
		 */
		for (childrow = tailrow, parentrow = decrement(childrow), parentturn = LOOKAHEAD & 1 ? -turn : turn;
		     parentrow != tailrow;
		     childrow = parentrow, parentrow = decrement(childrow), parentturn = -parentturn) { /* loop A */
			for (childcol = 0, parentcol = 0, minmax = parentturn == White ? -INF : INF;
			     parentcol < TREEWIDTH;
			     ++parentcol, minmax = parentturn == White ? -INF : INF) { /* loop B */
				for (i = 0; i < ftr_nchildren[TREEWIDTH * parentrow + parentcol]; ++i, ++childcol) { /* loop C */
					switch (parentturn) {
					case White: /* White is the maximizing player */
						if (minmax < whitescores[ftr_favored[TREEWIDTH * childrow + childcol]])
							minmax = whitescores[ftr_favored[TREEWIDTH * childrow + childcol]];
						break;
					case Black: /* Black is the minimizing player */
						if (minmax > blackscores[ftr_favored[TREEWIDTH * childrow + childcol]])
							minmax = blackscores[ftr_favored[TREEWIDTH * childrow + childcol]];
						break;
					}
				}
			}
		}
	} { /* Fourth, mark the now-obsolete predictions as erased, by settings their ftr_nchildren[i][j] = -1 */

	}

#undef INF
#undef increment
#undef decrement
}
